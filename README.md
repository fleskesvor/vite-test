# Vite Test

Simple web application developed with Vite, Vue 3, TypeScript and Sass, which I use to learn how to use Vite instead of Vue CLI + Webpack.

## Recommended IDEs

### [WebStorm](https://www.jetbrains.com/webstorm/) / [IntelliJ IDEA](https://www.jetbrains.com/idea/)

Without doubt the best IDE for Vue 3 with TypeScript. However, the Community Edition of IDEA does not support Vue, so your only options are paid and closed source.

### [VSCodium](https://vscodium.com/)

Free/Libre Open Source Software binaries of VS Code, without the Microsoft telemetry.

The following extensions are needed for the best developer experience:

- [Vue Language Features (Volar)](https://open-vsx.org/extension/johnsoncodehk/volar)
- [ESLint](https://open-vsx.org/extension/dbaeumer/vscode-eslint)
- [Prettier - Code formatter](https://open-vsx.org/extension/esbenp/prettier-vscode)
- [Sass](https://open-vsx.org/extension/syler/sass-indented)

**TIP #1:** If you're used to the JetBrains products, you will want to enable auto save to save your sanity. To do this, press `Ctrl+Shift+P`,
and start typing "toggle auto save", and then press `Enter` to enable auto save.

**TIP #2:** To have VSCodium use your `tsconfig.json` file for better IDE support, press `Ctrl+Shift+P`, and start typing "workspace settings json",
and then press `Enter` to open the settings file. Then set `typescript.tsdk` to a valid TypeScript path in `node_modules` and save:

```
{
    "typescript.tsdk": "node_modules/typescript/lib"
}
```

## Useful links

- [Add testing to Vite](https://dev.to/vuesomedev/add-testing-to-vite-4b75)
- [ESLint and Prettier with Vite and Vue.js 3](https://vueschool.io/articles/vuejs-tutorials/eslint-and-prettier-with-vite-and-vue-js-3/)
