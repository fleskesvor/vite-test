import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  // https://github.com/vitejs/vite/issues/1930
  const apiClient = mode === "development" ? "mock" : "server";
  return {
    plugins: [vue()],
    resolve: {
      alias: [
        { find: "@", replacement: new URL("/src", import.meta.url).pathname },
        { find: /~(.+)/, replacement: new URL("node_modules/$1", import.meta.url).pathname },
        { find: "api-client", replacement: new URL(`./src/api/${apiClient}`, import.meta.url).pathname },
      ],
    },
    css: {
      preprocessorOptions: {
        sass: {
          additionalData: `
            @import "~bulma/sass/utilities/_all.sass"
          `,
        },
      },
    },
    server: {
      proxy: {
        "/api": {
          target: "http://jsonplaceholder.typicode.com",
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ""),
        },
      },
    },
  };
});
