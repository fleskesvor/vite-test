import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Foo from "@/components/Foo.vue";
import Bar from "@/components/Bar.vue";
import Baz from "@/components/Baz.vue";

export enum Views {
  FOO = "FOO",
  BAR = "BAR",
  BAZ = "BAZ",
}

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: () => ({ name: Views.FOO }),
  },
  {
    name: Views.FOO,
    path: "/foo",
    component: Foo,
  },
  {
    name: Views.BAR,
    path: "/bar",
    component: Bar,
  },
  {
    name: Views.BAZ,
    path: "/baz",
    component: Baz,
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
