import { Post } from "@/domain/posts";

export const getTitle = (): string => "Vite Mock";

export const getPost = (id: number): Promise<Post> =>
  new Promise<Post>((resolve) =>
    resolve(<Post>{
      userId: id,
      id: 42,
      title: "Test",
      body: "Test, test, test!",
    })
  );
