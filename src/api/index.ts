// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { getTitle as getTitleImpl, getPost as getPostImpl } from "api-client";
import { Post } from "@/domain/posts";

export const getTitle = (): string => getTitleImpl();

export const getPost = (id: number): Promise<Post> => getPostImpl(id);
