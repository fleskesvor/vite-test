import { Post } from "@/domain/posts";
import axios, { AxiosResponse } from "axios";

const POSTS_API = "/api/posts";

export const getTitle = (): string => "Vite Server";

export const getPost = (id: number): Promise<Post> =>
  axios
    .get(`${POSTS_API}/${id}`)
    .then((response: AxiosResponse<Post>) => ({ ...response.data, body: response.data.body.split("\n")[0] }));
