declare module "@/styles/exports.module.sass" {
  type Styles = {
    brandYellow: string;
    brandGreenDark: string;
  };
  const styles: Styles;
  export default styles;
}
